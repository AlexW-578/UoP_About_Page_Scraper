# UoP_About_Page_Scraper

A small rust script which takes a html file and parses out people, links, and emails.

Made in Rust as that's what I'm teaching myself.

## Running:

``./web_scrap -- <Path to html file>``