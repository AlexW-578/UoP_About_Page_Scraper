use lazy_static::lazy_static;

fn find_all_emails(contents: &String, document_name: &str) {
    let re = Regex::new(r"\*@\*").unwrap();
    println!("Finding All the emails from {}:", document_name);
    for line in contents.split("\n") {
        if line.contains("@") {
            println!("{}", line);
            let login = extract_login(line);
            println!("{}", login);
        }
    }
    println!("{}", contents.len());
    println!("E");
}

fn extract_login(email: &str) -> &str {
    // regex pattern to extract login from email
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(?x)(?P<last_name>\S)+.(?P<first_name>\S)+@").unwrap();
    }
    let mut e = "";
    let login = RE.captures(email);
    match login {
        Some(login) => {
            e = login.name("email").unwrap().as_str();
        }
        None => e = "none found",
    }

    return e;
}