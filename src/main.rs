use reqwest;
use scraper;
use std::env;
use std::env::join_paths;
use std::fmt::format;
use std::fs;
use std::fs::File;
use std::io::Write;
use std::path::Path;
use regex::Regex;
use clap::Parser;

#[derive(Parser)]
struct Cli {
    html_file_path: String,
}

fn main() {
    let args = Cli::parse();
    let file_path = &args.html_file_path;
    if file_path.starts_with("/") == true {
        parse_file(file_path);
    } else {
        let number = 0;

        let file_path = download_webpage(file_path, number);
        parse_file(file_path.as_str());
    }
}

fn parse_file(file_path: &str) {
    let file_name = "data.txt";
    let mut file = File::create(file_name).expect("Cannot create data file");
    let contents = fs::read_to_string(file_path)
        .expect("Should have been able to read the file");
    let document = scraper::Html::parse_document(&*contents);
    let mut links: Vec<&str> = Vec::new();
    links = find_all_links(links, &document);

    writeln!(file, "\nPeople:\n").expect("Unable to write to file");
    let people = get_names(document.clone());
    for person in people {
        //println!("{}", link);
        writeln!(file, "Name: {}", person.name).expect("Unable to write to file");
        writeln!(file, "Email: {}", person.email).expect("Unable to write to file");
        writeln!(file, "Job Title: {}", person.title).expect("Unable to write to file");
        writeln!(file, "Department: {}", person.department).expect("Unable to write to file");
        writeln!(file, "Faculty: {}", person.faculty).expect("Unable to write to file");
        writeln!(file, "PHD Coordinator: {}\n", person.phd_supervisor).expect("Unable to write to file");
    }

    writeln!(file, "\nEmails:\n").expect("Unable to write to file");
    let emails = extract_emails_from_mailto(links.clone());
    for email in emails {
        //println!("{}", email);
        writeln!(file, "{}", email.to_string()).expect("Unable to write to file");
    }
    writeln!(file, "\nSubdomains of port.ac.uk:\n").expect("Unable to write to file");
    let subdomains = find_subdomains(links.clone(), ".port.ac.uk");
    for sub in subdomains {
        //println!("{}", sub);
        writeln!(file, "{}", sub).expect("Unable to write to file");
    }
    writeln!(file, "\nExternal Links:\n").expect("Unable to write to file");
    let mut internal = Vec::new();
    for link in links.clone() {
        //println!("{}", link);
        if link.starts_with("http") == false {
            internal.push(link)
        } else {
            writeln!(file, "{}", link).expect("Unable to write to file");
        }
    }
    writeln!(file, "\nInternal Links:\n").expect("Unable to write to file");
    for link in internal {
        //println!("{}", link);
        writeln!(file, "{}", link).expect("Unable to write to file");
    }
}

fn find_subdomains(links: Vec<&str>, domain: &str) -> Vec<String> {
    let mut port = Vec::new();
    for i in links {
        if i.contains(&domain) == true {
            if i.starts_with("https://") {
                let e: Vec<&str> = i.trim_start_matches("https://").split(".").collect();
                port.push(e[0].to_string());
            } else {
                let e: Vec<&str> = i.trim_start_matches("http://").split(".").collect();
                port.push(e[0].to_string());
            };
        }
    }
    return port;
}

fn find_all_links<'a>(mut links: Vec<&'a str>, document: &'a scraper::Html) -> Vec<&'a str> {
    let article_selector = scraper::Selector::parse("a").unwrap();
    for element in document.select(&article_selector) {
        let href = match element.value().attr("href") {
            Some(url) => url,
            _ => "",
        };
        //println!("{}",href);
        links.push(href);
    }
    return links;
}

fn extract_emails_from_mailto(lines: Vec<&str>) -> Vec<&str> {
    let mut emails: Vec<&str> = Vec::new();
    for line in lines {
        if line.starts_with("mailto:") == true {
            line.to_string();
            let e = line.trim_start_matches("mailto:");
            emails.push(e);
        }
    }
    return emails;
}


struct ABOUT {
    name: String,
    email: String,
    title: String,
    department: String,
    faculty: String,
    phd_supervisor: bool,
}

fn get_names(document: scraper::Html) -> Vec<ABOUT> {
    let mut people = Vec::new();
    let person_selector = scraper::Selector::parse("div.c-List-item-person").unwrap();
    let name_selector = scraper::Selector::parse("h3>a").unwrap();
    let title_selector = scraper::Selector::parse("ul>li").unwrap();
    for element in document.select(&person_selector) {
        let mut about: Vec<&str> = Vec::new();
        for name in element.select(&name_selector) {
            for text in name.text() {
                if text.trim().is_empty() == false {
                    about.push(text.trim());
                }
            }
        }
        for title in element.select(&title_selector) {
            for text in title.text() {
                if text.trim().is_empty() == false {
                    about.push(text.trim());
                }
            }
        }
        let person = ABOUT {
            name: about[0].to_string(),
            email: about[4].to_string(),
            title: about[2].to_string(),
            department: about[6].to_string(),
            faculty: about[8].to_string(),
            phd_supervisor: about.len() == 11,
        };
        people.push(person);
    }
    return people;
}

fn download_webpage(url: &str, number: i32) -> String {
    let response = reqwest::blocking::get(url).unwrap().text().unwrap();
    let path = env::current_dir().expect("E");
    let dir = format!("{}/html_files", path.to_str().expect("E"));
    if Path::new(dir.clone().as_str()).exists() == false {
        fs::create_dir("./html_files").expect("Could not create html file directory.");
    }
    let path = format!("{}/{}", dir, number);
    println!("{}", &path);
    let mut file = File::create(path.clone().as_str()).expect("Cannot create html file");
    write!(file, "{}", response).expect(&*format!("Could not write {} to file", &path));
    return path;
}